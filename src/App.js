import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {
    Button, Col,
    Collapse, DropdownItem, DropdownMenu, DropdownToggle,
    Input,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    NavItem,
    NavLink, Progress, Row, Table,
    UncontrolledDropdown
} from 'reactstrap';
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle,  } from 'reactstrap';


class App extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }


    render() {
        return (
            <div className="App">
                <Navbar color="danger" dark expand="md">
                    <NavbarBrand href="/">reactstrap</NavbarBrand>
                    <NavbarToggler />
                    <Collapse  navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink href="/components/">Components</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
                            </NavItem>
                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret>
                                    Options
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        Option 1
                                    </DropdownItem>
                                    <DropdownItem>
                                        Option 2
                                    </DropdownItem>
                                    <DropdownItem divider />
                                    <DropdownItem>
                                        Reset
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>

                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>

                    <div className={"container"}>
                        <Table className="myTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                            </tr>
                            </tbody>
                        </Table>


                        <p>
                            Edit <code>src/App.js</code> and save to reload.
                        </p>
                        <div className="inputDiv" >
                                <Input type="text" name="email" placeholder="with a placeholder" className="mr-2 col-md-4" />
                                <Button color ="danger">
                                    Ok
                                </Button>
                        </div>

                        <Progress multi className={"mt-5"}>
                            <Progress bar value="15" />
                            <Progress bar color="success" value="20" />
                            <Progress bar color="info" value="25" />
                            <Progress bar color="warning" value="20" />
                            <Progress bar color="danger" value="15" />
                        </Progress>
                        <div className="text-center">With Labels</div>
                        <Progress multi>
                            <Progress bar value="15">Meh</Progress>
                            <Progress bar color="success" value="35">Wow!</Progress>
                            <Progress bar color="warning" value="25">25%</Progress>
                            <Progress bar color="danger" value="25">LOOK OUT!!</Progress>
                        </Progress>
                        <div className="text-center">Stripes and Animations</div>
                        <Progress multi>
                            <Progress bar striped value="15">Stripes</Progress>
                            <Progress bar animated color="success" value="30">Animated Stripes</Progress>
                            <Progress bar color="info" value="25">Plain</Progress>
                        </Progress>


                        <Row className={"d-flex justify-content-center mt-5 mb-5"}>
                            <Col md={4}>
                                <Card >
                                    <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
                                    <CardBody>
                                        <CardTitle>Card title</CardTitle>
                                        <CardSubtitle>Card subtitle</CardSubtitle>
                                        <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                        <Button>Click</Button>
                                    </CardBody>
                                </Card>
                            </Col>

                            <Col md={4}>
                                <Card>
                                    <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
                                    <CardBody>
                                        <CardTitle>Card title</CardTitle>
                                        <CardSubtitle>Card subtitle</CardSubtitle>
                                        <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                        <Button>Click</Button>
                                    </CardBody>
                                </Card>
                            </Col>

                        </Row>

                    </div>


                </header>
            </div>

        );
    }
}

export default App;
